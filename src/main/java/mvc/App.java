package mvc;

import mvc.view.*;

public class App {

    public static void main(String[] args) {
        new MyView().show();
    }
}
