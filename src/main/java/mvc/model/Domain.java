package mvc.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Domain {

    private List<Integer> numberList;

    public Domain() {

        numberList = new LinkedList<>();
        numberList.add(3);
        numberList.add(4);
    }


    public List<Integer> getNumberList() {
        return numberList;
    }
}
