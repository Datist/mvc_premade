package mvc.model;

import java.util.List;

public class BusinessLogic implements Model {
    private  Domain domain;

    public  BusinessLogic() {
        domain = new Domain();
    }

    @Override
    public List<Integer> premadeNumberList() {
        return domain.getNumberList();
    }
}
