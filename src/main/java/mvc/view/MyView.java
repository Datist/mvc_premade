package mvc.view;

import mvc.controller.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);


    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - do 1");
        menu.put("2", "2 - do 2");
        menu.put("3", "3 - do 3");
        menu.put("4", "4 - do 4");
        menu.put("q", "q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("", this::pressButton1);
        methodsMenu.put("", this::pressButton2);
        methodsMenu.put("", this::pressButton3);
        methodsMenu.put("", this::pressButton4);
    }

    private void pressButton1() {
        // do 1
        System.out.println(controller.numberList());
    }

    private void pressButton2() {
        // do 1
    }

    private void pressButton3() {
        // do 1
    }

    private void pressButton4() {
        // do 1
    }

    // ----- main View method -------


    private void outputMenu() {
        System.out.println("\nMENU");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Choose what to do:");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));

    }
}
