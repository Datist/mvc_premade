package mvc.view;

@FunctionalInterface
public interface Printable {

    void print();
}
