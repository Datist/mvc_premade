package mvc.controller;

import mvc.model.BusinessLogic;
import mvc.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {
    private  Model model;
    public ControllerImpl(){
        model = new BusinessLogic();
    }


    @Override
    public List<Integer> numberList() {
        return model.premadeNumberList();
    }
}
